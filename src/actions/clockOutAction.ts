import puppeteer from 'puppeteer';
import infoConfig from '../configs/infoConfig';
import loggers from '../utils/logger';

class clockOutActionClass {
    public clockOut() : void{
        doClockOutActionPromise().then();
    }
}

async function doClockOutActionPromise():Promise<void> {
    const logger = new loggers();
    const browser = await puppeteer.launch({headless: true});
    const page = (await browser.pages())[0];

    try {

        //登入打卡系統
        await page.goto('https://cloud.nueip.com/login');

        const deptInput = await page.$('#dept_input')
        if (deptInput) {
            await deptInput.focus();
            await deptInput?.type(infoConfig.dept); // 公司編號
        }
        const userNameInput = await page.$('#username_input');
        if (userNameInput) {
            await userNameInput.focus();
            await userNameInput?.type(infoConfig.username); //工號
        }
        const passwordInput = await page.$('#password-input')
        if (passwordInput) {
            await passwordInput.focus();
            await passwordInput?.type(infoConfig.password); //密碼
        }
        const loginBtn = await page.$('#login-button')
        if (loginBtn) {
            await loginBtn.click();
        }

        await page.waitForNavigation();
        logger.log("Is to Logined !");

        //抓取打卡系統日期
        let day_element = await page.$('#mm-0 > div.container > div > div.col-xs-12.col-lg-10.desktopsite_right > div.row > div.col-xs-12.col-lg-3 > div:nth-child(1) > div.panel.clock_blcok > div > div.date > div.today');
        let today = await page.evaluate(el => el.textContent, day_element);

        logger.log(`Today is ${today}`);

        //排除假日
        if (today.match('星期六') != null || today.match('星期日') != null) {
            logger.log('假日不執行! 關閉遊覽器');
            await browser.close();
            return;
        }

        //區間延遲1秒
        let delay = getRandom(10)*1000;
        logger.log(`等待區間延遲 : ${delay} 毫秒`);

        await page.waitForTimeout(delay);

        //下班
        const clockOutBtn = await page.$('#clockout')
        if (clockOutBtn) {
            await clockOutBtn.click()
        }

        logger.log(`已成功執行下班打卡! 打卡時間為: ${new Date()} ，關閉遊覽器`);
        await browser.close();
    }catch (e : any) {
        logger.error(e.message);
        await browser.close();
    }
};

export = clockOutActionClass;

function getRandom( x : number){
    return Math.floor(Math.random()*x)+1;
};