enum logConfig {
    flags = 'a',
    encoding = 'utf8',
    useConsoleLog = "1",
    recordPath = './src/logs/record.log',
    ErrorPath = './src/logs/error.log',
};

export default logConfig;