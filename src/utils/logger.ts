import logConfig from '../configs/logConfig';
import telegramBot from '../utils/telegramBot';

const fs = require('fs');
const bot = new telegramBot();

let options = {
    flags: logConfig.flags,
    encoding: logConfig.encoding,
}

let recordStderr = fs.createWriteStream(logConfig.recordPath, options);
let recordLog = new console.Console(recordStderr);

let ErrorStderr = fs.createWriteStream(logConfig.ErrorPath, options);
let ErrorLog = new console.Console(ErrorStderr);

class logger {

    private readonly useConsoleLog: string;

    constructor() {
        this.useConsoleLog = logConfig.useConsoleLog;
    }

    public log(message: string) : void {
        recordLog.log(new Date(Date.parse(new Date().toString()))+ message);
        bot.send(message);
        if (this.useConsoleLog == '1'){
            console.log(message);
        }
    }

    public error(message: string) : void {
        recordLog.log(new Date(Date.parse(new Date().toString()))+ message);
        ErrorLog.log(new Date(Date.parse(new Date().toString())) + message);
        bot.send(message);
        if (this.useConsoleLog){
            console.error(message);
        }
    }

    public logByInfo(message: string) : void {
        recordLog.log(new Date(Date.parse(new Date().toString()))+ message);
        if (this.useConsoleLog == '1'){
            console.log(message);
        }
    }
}

export = logger;