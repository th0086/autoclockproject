import telegramConfig from '../configs/telegramConfig';
import { Telegraf } from 'telegraf';

const app = new Telegraf(telegramConfig.token);

class tgMsg {

    private readonly enable: string;

    constructor() {
        this.enable = telegramConfig.enable;
    }

    public send(message : string) : void{
        if (this.enable == '1'){
            return;
        }
        app.telegram.sendMessage(telegramConfig.chatId, message).then();
    }
}

export = tgMsg;