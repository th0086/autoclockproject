const path = require('path');
const fs = require('fs');

const directoryPath = path.join(__dirname, 'servers');

fs.readdir(directoryPath, (err : any, files : string[]) => {
    files.forEach((file) => {
        const serversName =file.replace('.ts', '');
        require(`./servers/${serversName}`)();
    });
});