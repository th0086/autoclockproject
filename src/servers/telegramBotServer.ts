import telegramConfig from '../configs/telegramConfig';
import { Telegraf } from 'telegraf';
import loggers from '../utils/logger';
import clockInActionClass from '../actions/clockInAction';
import clockOutActionClass from '../actions/clockOutAction';

const bot = new Telegraf(telegramConfig.token);
const logger = new loggers();
const clockInAction = new clockInActionClass();
const clockOutAction = new clockOutActionClass();

module.exports = ()=> {

    bot.command('start', ctx => {
        logger.logByInfo(`ID: ${ctx.from.id};First_name: ${ctx.from.first_name};Last_name: ${ctx.from.last_name} command : start;`);
        bot.telegram.sendMessage(ctx.chat.id, '歡迎來到艾倫打屁屁廣播~').then()
    });

    bot.command('autoClock', ctx => {
        logger.logByInfo(`ID: ${ctx.from.id};First_name: ${ctx.from.first_name};Last_name: ${ctx.from.last_name} command : autoClock;`);
        let autoClockMessage = `哈囉，請選擇你要打卡上班還是打卡下班，注意!! 請勿按錯鍵!`;
        bot.telegram.sendMessage(ctx.chat.id, autoClockMessage, {
            reply_markup: {
                inline_keyboard: [
                    [{
                        text: "打卡上班",
                        callback_data: 'clockIn'
                    },
                        {
                            text: "打卡下班",
                            callback_data: 'clockOut'
                        }
                    ],

                ]
            }
        }).then()
    });

    bot.action('clockIn', ctx => {
        logger.log('已觸發按鍵clockIn');
        clockInAction.clockIn();
    });

    bot.action('clockOut', ctx => {
        logger.log('已觸發按鍵clockOut');
        clockOutAction.clockOut();
    });

    bot.launch().then(() => logger.log(`BotServer已成功啟動`));
}